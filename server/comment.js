const { REQ_PATH, PC_HOST } = require('./config')
const axios = require('axios')
const router = require('koa-router')()

// 获取更多原评论的回复评论数据
router.get('/more-reply', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/v2/reply/reply/cursor`
    const qs = ctx.request.querystring.replace('type', 'pageType')
    const res = await axios.get(url, {
      headers: {
        referer: `${PC_HOST}/h5/comment/sub?${qs}`,
        origin: PC_HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

module.exports = router
