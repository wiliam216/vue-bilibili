const { REQ_PATH, REF_PATH, HOST } = require('./config')
const axios = require('axios')
const router = require('koa-router')()

// 获取排行数据
router.get('/get', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/web-interface/ranking`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/ranking`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})
module.exports = router
