const { REQ_PATH, REF_PATH } = require('./config')
const axios = require('axios')
const router = require('koa-router')()

// 获取各种类型数据(不分页)
router.get('/ranking', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/web-interface/ranking/region`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/channel/${ctx.request.query.rid}`,
        origin: REF_PATH
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {}
  await next()
})

// 获取各种类型数据(分页)
router.get('/dynamic', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/web-interface/dynamic/region`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/channel/${ctx.request.query.id}/${ctx.request.query.subid}`,
        origin: REF_PATH
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {}
  await next()
})

module.exports = router
