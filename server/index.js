const { REQ_PATH, REF_PATH, HOST } = require('./config')
const axios = require('axios')
const router = require('koa-router')()

// 获取首页轮播图
router.get('/getSlider', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/web-show/res/loc`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/index.html`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {}
  await next()
})

// 获取首页推荐视频
router.get('/getRecommend', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/web-interface/ranking`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/index.html`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {}
  await next()
})

module.exports = router
