const { REQ_PATH, TOPIC_HOST } = require('./config')
const axios = require('axios')
const router = require('koa-router')()
const api = 'https://api.vc.bilibili.com/topic_svr'
// 获取话题基本信息
router.get('/info', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/tag/info`
    const res = await axios.get(url, {
      headers: {
        referer: `${TOPIC_HOST}/h5/dynamic/topic?topic_name=${encodeURIComponent(ctx.request.query.tag_name)}`,
        origin: TOPIC_HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取话题的活跃用户
router.get('/active_users', async (ctx, next) => {
  try {
    const url = `${api}/v1/topic_svr/get_active_users`
    const res = await axios.get(url, {
      headers: {
        referer: `${TOPIC_HOST}/h5/dynamic/topic?topic_name=${encodeURIComponent(ctx.request.query.topic_name)}`,
        origin: TOPIC_HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取话题相关的动态
router.get('/about', async (ctx, next) => {
  try {
    const url = `${api}/v1/topic_svr/fetch_dynamics`
    const res = await axios.get(url, {
      headers: {
        referer: `${TOPIC_HOST}/h5/dynamic/topic?topic_name=${encodeURIComponent(ctx.request.query.topic_name)}`,
        origin: TOPIC_HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

module.exports = router
