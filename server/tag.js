const { REQ_PATH, REF_PATH, HOST } = require('./config')
const axios = require('axios')
const router = require('koa-router')()
// 获取视频的标签相关信息
router.get('/getInfo', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/tag/info`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/tag/${ctx.request.query.tid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取视频标签的相关标签
router.get('/getSimilar', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/tag/change/similar`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/tag/${ctx.request.query.tid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取标签的相关视频
router.get('/getVideos', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/web-interface/tag/top`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/tag/${ctx.request.query.tid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})
module.exports = router
