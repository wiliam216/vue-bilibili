const { REQ_PATH, REF_PATH, HOST } = require('./config')
const axios = require('axios')
const router = require('koa-router')()

// 获取视频详细信息
router.get('/getDetail', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/web-interface/view`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/video/${ctx.request.query.bvid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {}
  await next()
})

// 获取视频内容相关的视频
router.get('/getRelated', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/web-interface/archive/related`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/video/${ctx.request.query.bvid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取视频评论
router.get('/getComment', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/v2/reply/main`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/video/${ctx.request.query.bvid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取视频标签
router.get('/getTag', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/tag/archive/tags`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/video/${ctx.request.query.bvid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取视频播放地址
router.get('/getPlayUrl', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/player/playurl`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/video/${ctx.request.query.bvid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取视频弹幕 抓取不到，暂时屏蔽
router.get('/getDanmu', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/v1/dm/list.so`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/video/${ctx.request.query.bvid}`,
        host: HOST
      },
      params: {
        oid: ctx.request.query.oid
      }
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

module.exports = router
