const { REQ_PATH, PC_HOST } = require('./config')
const axios = require('axios')
const router = require('koa-router')()
const cheerio = require('cheerio')

// 获取文章内容
router.get('/content', async (ctx, next) => {
  try {
    const url = `${PC_HOST}/read/mobile/${ctx.request.query.id}`
    const res = await axios.get(url)
    // console.log(res.data)
    res.data = res.data.replace(/<p><br\/><\/p>/g, '')
    res.data = res.data.replace(/data-src/g, 'src')
    const $ = cheerio.load(res.data, { decodeEntities: false })
    let result = {}
    if ($('body').length) {
      const type = $('.info').eq(0).find('span').eq(0).html()
      const timestamp = $('.create-time').eq(0).data('ts')
      let content = $('.article-holder').eq(0).html()
      const copyright = $('.authority').eq(0).html()
      const videoIds = []
      $('.video-card').each((i, n) => {
        $(n.parent).addClass('video-wrap')
        const ids = n.attribs.aid.split(',')
        const newid = ids.map(id => `av${id}`)
        videoIds.push(newid.join(','))
        $(n).after(`<i data-ids="${newid.join(',')}"></i>`)
        $(n).remove()
      })
      const tags = []
      $('.tag-item').each((i, n) => {
        const id = n.attribs['data-tag-id']
        const name = $(n).find('.tag-content').text()
        tags.push({
          id,
          name
        })
      })
      content = $('.article-holder').eq(0).html() + '<p class="copyright">' + $('.authority').html() + '</p>'
      result = {
        code: 0,
        data: {
          type,
          timestamp,
          content,
          copyright,
          videoIds,
          tags
        },
        message: '解析成功'
      }
    } else {
      result = { code: -1, data: null, message: '解析失败' }
    }
    ctx.response.body = result
  } catch (e) {
    console.log(e)
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取文章相关信息
router.get('/about', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/article/viewinfo`
    const res = await axios.get(url, {
      headers: {
        referer: `${PC_HOST}/read/mobile/${ctx.request.query.id}`,
        origin: PC_HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {}
  await next()
})

// 文章内容里面的视频
router.get('/videos', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/article/cards`
    const res = await axios.get(url, {
      headers: {
        referer: `${PC_HOST}/read/mobile/${ctx.request.query.id}`,
        origin: PC_HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 更多文章推荐
router.get('/more', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/article/more`
    const res = await axios.get(url, {
      headers: {
        referer: `${PC_HOST}/read/mobile/${ctx.request.query.id}`,
        origin: PC_HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 文章的评论内容
router.get('/comments', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/v2/reply/main`
    const res = await axios.get(url, {
      headers: {
        referer: `${PC_HOST}/read/mobile/${ctx.request.query.oid}`,
        origin: PC_HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

module.exports = router
