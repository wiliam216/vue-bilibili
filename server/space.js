const { REQ_PATH, REF_PATH, HOST } = require('./config')
const axios = require('axios')
const router = require('koa-router')()

// 获取用户信息
router.get('/getUserInfo', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/space/acc/info`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/space/${ctx.request.query.mid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取用户的粉丝数和关注数
router.get('/getUserStat', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/relation/stat`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/space/${ctx.request.query.vmid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取用户获得的点赞数
router.get('/getUserUpStat', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/space/upstat`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/space/${ctx.request.query.mid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取用户主页的最新动态列表
router.get('/getUserPosts', async (ctx, next) => {
  try {
    const url = 'https://api.vc.bilibili.com/dynamic_svr/v1/dynamic_svr/space_history'
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/space/${ctx.request.query.host_uid}`,
        host: 'api.vc.bilibili.com'
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取用户主页的视频列表
router.get('/getUserVideos', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/space/arc/search`
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/space/${ctx.request.query.mid}`,
        host: HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取用户主页的最新动态列表
router.get('/getUserPhotos', async (ctx, next) => {
  try {
    const url = 'https://api.vc.bilibili.com/link_draw/v1/Doc/photo_list_ones'
    const res = await axios.get(url, {
      headers: {
        referer: `${REF_PATH}/space/${ctx.request.query.uid}`,
        host: 'api.vc.bilibili.com'
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})
module.exports = router
