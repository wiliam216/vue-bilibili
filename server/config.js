const config = {
  REQ_PATH: 'https://api.bilibili.com/x',
  REF_PATH: 'https://m.bilibili.com',
  API_HOST: 'https://api.bilibili.com',
  PC_HOST: 'https://www.bilibili.com',
  TOPIC_HOST: 'https://t.bilibili.com',
  HOST: 'api.bilibili.com'
}

module.exports = config
