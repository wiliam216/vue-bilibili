const { REQ_PATH, TOPIC_HOST } = require('./config')
const axios = require('axios')
const router = require('koa-router')()

// 获取更多原评论的回复评论数据
router.get('/vote_info', async (ctx, next) => {
  try {
    const url = 'https://api.vc.bilibili.com/vote_svr/v1/vote_svr/vote_info'
    const res = await axios.get(url, {
      headers: {
        referer: `${TOPIC_HOST}/vote/h5/index/`,
        origin: TOPIC_HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

// 获取视频评论
router.get('/comment', async (ctx, next) => {
  try {
    const url = `${REQ_PATH}/v2/reply/main`
    const res = await axios.get(url, {
      headers: {
        referer: `${TOPIC_HOST}/vote/h5/index/`,
        origin: TOPIC_HOST
      },
      params: ctx.request.query
    })
    ctx.response.body = res.data
  } catch (e) {
    ctx.response.body = '出错了'
  }
  await next()
})

module.exports = router
