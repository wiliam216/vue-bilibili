const routes = [
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/404.vue'),
    meta: {
      headerVisible: true,
      menuVisible: false
    }
  },
  {
    path: '/',
    name: 'index',
    component: () => import('@/views/Index.vue'),
    meta: {
      headerVisible: true,
      menuVisible: true
    }
  },
  {
    path: '/channel/:id/:subid?',
    name: 'channel',
    props: true,
    component: () => import('@/views/Channel.vue'),
    meta: {
      headerVisible: true,
      menuVisible: true
    }
  },
  {
    path: '/video/:bvid+',
    name: 'video',
    props: true,
    component: () => import('@/views/Video.vue'),
    meta: {
      headerVisible: true,
      menuVisible: false
    }
  },
  {
    path: '/bangumi/:id+',
    name: 'bangumi',
    props: true,
    component: () => import('@/views/Bangumi.vue'),
    meta: {
      headerVisible: true,
      menuVisible: false
    }
  },
  {
    path: '/tag/:tid+',
    name: 'tag',
    props: true,
    component: () => import('@/views/Tag.vue'),
    meta: {
      headerVisible: true,
      menuVisible: false
    }
  },
  {
    path: '/space/:mid+',
    name: 'space',
    props: true,
    component: () => import('@/views/Space.vue'),
    meta: {
      headerVisible: true,
      menuVisible: false
    }
  },
  {
    path: '/topic/:name+',
    name: 'topic',
    props: true,
    component: () => import('@/views/Topic.vue'),
    meta: {
      headerVisible: true,
      menuVisible: false
    }
  },
  {
    path: '/vote/:vid+',
    name: 'vote',
    props: true,
    component: () => import('@/views/Vote.vue'),
    meta: {
      headerVisible: true,
      menuVisible: false
    }
  },
  {
    path: '/article/:id+',
    name: 'article',
    props: true,
    component: () => import('@/views/Article.vue'),
    meta: {
      headerVisible: true,
      menuVisible: false
    }
  },
  {
    path: '/rank/:id?',
    name: 'rank',
    props: true,
    component: () => import('@/views/Rank.vue'),
    meta: {
      headerVisible: false,
      menuVisible: false
    }
  },
  {
    path: '/search',
    name: 'search',
    props: (route) => ({ keyword: route.query.keyword }),
    component: () => import('@/views/Search.vue'),
    meta: {
      headerVisible: false,
      menuVisible: false
    }
  },
  {
    // 找不到页面的时候的配置
    path: '*',
    name: '*',
    redirect: '/404'
  }
]

export default routes
