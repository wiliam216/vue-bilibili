export default [
  {
    name: '首页',
    route: '/'
  },
  {
    name: '动画',
    route: 'douga',
    tid: 1,
    sub: [
      {
        name: 'MAD·AMV',
        route: 'mad',
        tid: 24
      },
      {
        name: 'MMD·3D',
        route: 'mmd',
        tid: 25
      },
      {
        name: '短片·手书·配音',
        route: 'voice',
        tid: 47
      },
      {
        name: '手办·模玩',
        route: 'toy',
        tid: 210
      },
      {
        name: '特摄',
        route: 'tokusatsu',
        tid: 86
      },
      {
        name: '综合',
        route: 'other',
        tid: 27
      }
    ]
  },
  {
    name: '番剧',
    route: 'anime',
    tid: 13,
    sub: [
      {
        name: '连载动画',
        tid: 33,
        route: 'serial'
      },
      {
        name: '完结动画',
        tid: 32,
        route: 'finish'
      },
      {
        name: '资讯',
        tid: 51,
        route: 'information'
      },
      {
        name: '官方延伸',
        tid: 152,
        route: 'offical'
      }
    ]
  },
  {
    name: '国创',
    tid: 167,
    route: 'guochuang',
    sub: [
      {
        name: '国产动画',
        tid: 153,
        route: 'chinese'
      },
      {
        name: '国产原创相关',
        tid: 168,
        route: 'original'
      },
      {
        name: '布袋戏',
        tid: 169,
        route: 'puppetry'
      },
      {
        name: '动态漫·广播剧',
        tid: 195,
        route: 'motioncomic'
      },
      {
        name: '资讯',
        tid: 170,
        route: 'information'
      }
    ]
  },
  {
    name: '音乐',
    route: 'music',
    tid: 3,
    sub: [
      {
        name: '原创音乐',
        route: 'original',
        tid: 28
      },
      {
        name: '翻唱',
        route: 'cover',
        tid: 31
      },
      {
        name: 'VOCALOID·UTAU',
        route: 'vocaloid',
        tid: 30
      },
      {
        name: '电音',
        route: 'electronic',
        tid: 194
      },
      {
        name: '演奏',
        route: 'perform',
        tid: 59
      },
      {
        name: 'MV',
        route: 'mv',
        tid: 193
      },
      {
        name: '音乐现场',
        route: 'live',
        tid: 29
      },
      {
        name: '音乐综合',
        route: 'other',
        tid: 130
      }
    ]
  },
  {
    name: '舞蹈',
    route: 'dance',
    tid: 129,
    sub: [
      {
        name: '宅舞',
        route: 'otaku',
        tid: 20
      },
      {
        name: '街舞',
        route: 'hiphop',
        tid: 198
      },
      {
        name: '明星舞蹈',
        route: 'star',
        tid: 199
      },
      {
        name: '中国舞',
        route: 'china',
        tid: 200
      },
      {
        name: '舞蹈综合',
        route: 'three_d',
        tid: 154
      },
      {
        name: '舞蹈教程',
        route: 'demo',
        tid: 156
      }
    ]
  },
  {
    name: '游戏',
    route: 'game',
    tid: 4,
    sub: [
      {
        name: '单机游戏',
        route: 'stand_alone',
        tid: 17
      },
      {
        name: '电子竞技',
        route: 'esports',
        tid: 171
      },
      {
        name: '手机游戏',
        route: 'mobile',
        tid: 172
      },
      {
        name: '网络游戏',
        route: 'online',
        tid: 65
      },
      {
        name: '桌游棋牌',
        route: 'board',
        tid: 173
      },
      {
        name: 'GMV',
        route: 'gmv',
        tid: 121
      },
      {
        name: '音游',
        route: 'music',
        tid: 136
      },
      {
        name: 'Mugen',
        route: 'mugen',
        tid: 19
      }
    ]
  },
  {
    name: '知识',
    route: 'technology',
    tid: 36,
    sub: [
      {
        name: '科学科普',
        route: 'science',
        tid: 201
      },
      {
        name: '社科人文',
        route: 'fun',
        tid: 124
      },
      {
        name: '财经',
        route: 'economic',
        tid: 207
      },
      {
        name: '校园学习',
        route: 'school',
        tid: 208
      },
      {
        name: '职业职场',
        route: 'work',
        tid: 209
      },
      {
        name: '野生技术协会',
        route: 'wild',
        tid: 122
      }
    ]
  },
  {
    name: '数码',
    route: 'digital',
    tid: 188,
    sub: [
      {
        name: '手机平板',
        route: 'mobile',
        tid: 95
      },
      {
        name: '电脑装机',
        route: 'pc',
        tid: 189
      },
      {
        name: '摄影摄像',
        route: 'photography',
        tid: 190
      },
      {
        name: '影音智能',
        route: 'intelligence_av',
        tid: 191
      }
    ]
  },
  {
    name: '生活',
    route: 'life',
    tid: 160,
    sub: [
      {
        name: '搞笑',
        route: 'funny',
        tid: 138
      },
      {
        name: '日常',
        route: 'daily',
        tid: 21
      },
      {
        name: '美食圈',
        route: 'food',
        tid: 76
      },
      {
        name: '动物圈',
        route: 'animal',
        tid: 75
      },
      {
        name: '手工',
        route: 'handmake',
        tid: 161
      },
      {
        name: '绘画',
        route: 'painting',
        tid: 162
      },
      {
        name: '运动',
        route: 'sports',
        tid: 163
      },
      {
        name: '汽车',
        route: 'automobile',
        tid: 176
      },
      {
        name: '其他',
        route: 'other',
        tid: 174
      }
    ]
  },
  {
    name: '鬼畜',
    route: 'kichiku',
    tid: 119,
    sub: [
      {
        name: '鬼畜调教',
        route: 'guide',
        tid: 22
      },
      {
        name: '音MAD',
        route: 'mad',
        tid: 26
      },
      {
        name: '人力VOCALOID',
        route: 'manual_vocaloid',
        tid: 126
      },
      {
        name: '教程演示',
        route: 'course',
        tid: 127
      }
    ]
  },
  {
    name: '时尚',
    route: 'fashion',
    tid: 155,
    sub: [
      {
        name: '美妆',
        route: 'makeup',
        tid: 157
      },
      {
        name: '服饰',
        route: 'clothing',
        tid: 158
      },
      {
        name: '健身',
        route: 'aerobics',
        tid: 164
      },
      {
        name: 'T台',
        route: 'catwalk',
        tid: 159
      },
      {
        name: '风尚标',
        route: 'trends',
        tid: 192
      }
    ]
  },
  {
    name: '娱乐',
    route: 'ent',
    tid: 5,
    sub: [
      {
        name: '综艺',
        route: 'variety',
        tid: 71
      },
      {
        name: '明星',
        route: 'star',
        tid: 137
      }
    ]
  },
  {
    name: '影视',
    route: 'cinephile',
    tid: 181,
    sub: [
      {
        name: '影视杂谈',
        route: 'cinecism',
        tid: 182
      },
      {
        name: '影视剪辑',
        route: 'montage',
        tid: 183
      },
      {
        name: '短片',
        route: 'shortfilm',
        tid: 85
      },
      {
        name: '预告·资讯',
        route: 'trailer_info',
        tid: 184
      }
    ]
  },
  {
    name: '资讯',
    route: 'news',
    tid: 202,
    hide: !0,
    sub: [
      {
        name: '热点',
        tid: 203,
        route: 'hotspot',
        hide: !0
      },
      {
        name: '环球',
        tid: 204,
        route: 'international',
        hide: !0
      },
      {
        name: '社会',
        tid: 205,
        route: 'social',
        hide: !0
      },
      {
        name: '综合',
        tid: 206,
        route: 'multiple',
        hide: !0
      }
    ]
  },
  {
    name: '纪录片',
    route: 'documentary',
    tid: 177,
    sub: [
      {
        name: '人文·历史',
        tid: 37,
        route: 'history'
      },
      {
        name: '科学·探索·自然',
        tid: 178,
        route: 'science'
      },
      {
        name: '军事',
        tid: 179,
        route: 'military'
      },
      {
        name: '社会·美食·旅行',
        tid: 180,
        route: 'travel'
      }
    ]
  },
  {
    name: '电影',
    route: 'movie',
    tid: 23,
    sub: [
      {
        name: '华语电影',
        tid: 147,
        route: 'chinese'
      },
      {
        name: '欧美电影',
        tid: 145,
        route: 'west'
      },
      {
        name: '日本电影',
        tid: 146,
        route: 'japan'
      },
      {
        name: '其他国家',
        tid: 83,
        route: 'movie'
      }
    ]
  },
  {
    name: '电视剧',
    route: 'tv',
    tid: 11,
    sub: [
      {
        name: '国产剧',
        tid: 185,
        route: 'mainland'
      },
      {
        name: '海外剧',
        tid: 187,
        route: 'overseas'
      }
    ]
  }
]
