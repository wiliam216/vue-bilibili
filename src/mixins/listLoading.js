export default {
  data () {
    return {
      listLoading: true,
      listLoadingText: '加载中...',
      listFinished: false,
      listFinishedText: '没有更多了',
      listError: false,
      listErrorText: '请求失败，点击重新加载',
      listTimeout: null,
      listImmediateCheck: false
    }
  },
  methods: {
    resetListStatus () {
      this.listLoading = true
      this.listFinished = false
      this.listError = false
      this.listTimeout = null
    }
  }
}
