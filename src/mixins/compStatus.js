// 公用mixin
export const compStatus = {
  data () {
    return {
      // 公用的flag判断组件是否存活 用来处理keep-live情况下 watch监听会保存并重复触发的
      isCompAlive: false
    }
  },
  mounted () {
    this.isCompAlive = true
  },
  activated () {
    this.isCompAlive = true
  },
  deactivated () {
    this.isCompAlive = false
  }
}
