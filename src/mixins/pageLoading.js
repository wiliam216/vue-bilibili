// 公用mixin
export default {
  data () {
    return {
      // 页面加载状态 默认false
      pageLoading: true,
      // 页面加载是否出错 默认false
      pageError: false
    }
  }
}
