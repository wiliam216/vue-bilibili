import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueMeta from 'vue-meta'
import { Lazyload } from 'vant'
import FastClick from 'fastclick'
Vue.config.productionTip = false
FastClick.attach(document.body)
Vue.use(Lazyload)
Vue.use(VueMeta)
new Vue({
  render: h => h(App),
  router
}).$mount('#app')
