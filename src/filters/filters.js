// 图片原图加上尺寸限制 输出对应大小的图片
export function formatImage (url, size) {
  // size = size.replace('.webp', '')
  return `${url}${size}`
}
// 时间戳转换成 月-日 格式日期
export function formatDate (value) {
  const dateobj = new Date(value * 1000)
  let month = dateobj.getMonth() + 1
  let date = dateobj.getDate()
  month = (month + '').padStart(2, '0')
  date = (date + '').padStart(2, '0')
  return `${month}-${date}`
}
// 数字转换为四舍五入小数 123456 => 12.3万
export function formatNumber (value) {
  if (value < 10000) {
    return value
  } else {
    let num = value / 10000
    let yi = num / 10000
    let str = ''
    if (yi >= 1) {
      // 亿
      yi = Math.round(yi * Math.pow(10, 1)) / Math.pow(10, 1)
      yi = Number(yi).toFixed(1)
      str = `${yi - 0}亿`
    } else {
      // 万
      // 四舍五入
      num = Math.round(num * Math.pow(10, 1)) / Math.pow(10, 1)
      // 补足位数
      num = Number(num).toFixed(1)
      str = `${num - 0}万`
    }
    return str
  }
}
// 格式化时间 165 => 2:45
export function formatTime (times) {
  if (Object.prototype.toString.call(times) === '[object String]') {
    return times
  } else {
    const hour = Number.parseInt(times / (60 * 60))
    const mini = Number.parseInt((times % (60 * 60)) / 60)
    let second = 0
    if (hour === 0 && mini === 0) {
      second = times
    } else {
      second = times % (hour * 60 * 60 + mini * 60)
    }
    second = (second + '').padStart(2, '0')
    return `${hour > 0 ? (hour + ':') : ''}${mini}:${second}`
  }
}

// 距离现在多久的时间
export function howLong (value) {
  const postTime = value * 1000
  const postDate = new Date(postTime)
  const now = new Date()
  const nowYear = now.getFullYear()
  const nowTime = now.getTime()
  const gapTime = nowTime - postTime
  const lastYear = postDate.getFullYear()
  const month = postDate.getMonth() + 1
  const date = postDate.getDate()
  const formatMonth = (month + '').padStart(2, '0')
  const formatDate = (date + '').padStart(2, '0')
  const onedayTime = 24 * 60 * 60 * 1000
  const yesterday = new Date(nowTime)
  yesterday.setDate(yesterday.getDate() - 1)
  const ydate = yesterday.getDate()
  let fd = ''
  if (gapTime <= onedayTime) {
    // 一天之内的就展示几小时以前
    const oneHour = 60 * 60 * 1000
    if (gapTime < oneHour) {
      const oneSecond = 60 * 1000
      fd = `${Number.parseInt(gapTime / oneSecond)}分钟前`
    } else {
      fd = `${Number.parseInt(gapTime / oneHour)}小时前`
    }
  } else if (ydate === date) {
    // 显示昨天
    fd = '昨天'
  } else if (nowYear !== lastYear) {
    fd = `${lastYear}-${formatMonth}-${formatDate}`
  } else {
    fd = `${formatMonth}-${formatDate}`
  }
  return fd
}
