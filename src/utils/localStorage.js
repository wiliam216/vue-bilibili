function setStorage (key, val) {
  window.localStorage.setItem(key, JSON.stringify(val))
}

function getStorage (key) {
  const vals = window.localStorage.getItem(key)
  return vals ? JSON.parse(vals) : []
}

function removeStorage (key) {
  window.localStorage.removeItem(key)
}

function clear () {
  window.localStorage.clear()
}

export {
  setStorage,
  getStorage,
  removeStorage,
  clear
}
