// 话题页数据接口
import request from '@/utils/http.js'
/* 获取话题的基本信息
 * @params[String] url 请求地址
 * @params[String] tagName 话题关键字
 * @return [Promise]
 */
function apiGetInfo ({ url = '/topic/info', tagName } = {}) {
  return request.get(url, {
    data: {
      tag_name: tagName
    }
  })
}

/* 获取话题的活跃用户
 * @params[String] url 请求地址
 * @params[Number] topicId 话题id
 * @return [Promise]
 */
function apiGetActiveUsers ({ url = '/topic/active_users', topicId } = {}) {
  return request.get(url, {
    data: {
      topic_id: topicId
    }
  })
}

/* 获取话题的相关动态
 * @params[String] url 请求地址
 * @params[String] topicName 话题关键字
 * @params[Number] topicId 话题id
 * @params[Number] sortby 话题排序 2是按照时间
 * @params[String] platform 平台
 * @params[Number,String] offset 下一页的起始id
 * @return [Promise]
 */
function apiGetAbout ({ url = '/topic/about', topicName, topicId, sortby = 2, platform = 'h5', offset } = {}) {
  return request.get(url, {
    data: {
      topic_name: topicName,
      topic_id: topicId,
      sortby,
      platform,
      offset
    }
  })
}

export {
  apiGetInfo,
  apiGetActiveUsers,
  apiGetAbout
}
