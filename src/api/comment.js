// 评论数据接口
import request from '@/utils/http.js'

/* 获取评论的更多回复数据
 * @params[String] url 请求地址
 * @params[Number] oid 评论所属的内容id
 * @params[Number] type 评论所属的内容类型id
 * @params[Number] root 评论id
 * @params[Number] pageIndex 分页索引
 * @return [Promise]
 */
function apiGetMoreReply ({ url = '/comment/more-reply', oid, type, root, pageIndex } = {}) {
  return request.get(url, {
    data: {
      oid,
      type,
      root,
      min_id: pageIndex
    }
  })
}
export {
  apiGetMoreReply
}
