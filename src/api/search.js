// 搜索页数据接口
import request from '@/utils/http.js'
/* 获取搜索框显示的默认关键字
 * @params[String] url 请求地址
 * @return [Promise]
 */
function apiGetDefaultKey ({ url = '/search/getDefaultKey' } = {}) {
  return request.get(url)
}

/* 获取搜索框显示的默认关键字
 * @params[String] url 请求地址
 * @return [Promise]
 */
function apiGetHotSearchKey ({ url = '/search/getHotKey' } = {}) {
  return request.get(url)
}

/* 获取搜索框输入关键字所显示的搜索建议
 * @params[String] url 请求地址
 * @params[String] func 接口
 * @params[String] suggestType 建议类型 默认 accurate
 * @params[String] subType 子类型 默认 tag
 * @params[String] mainVer 版本 默认 v1
 * @params[String] highlight 高亮 默认''
 * @params[Number] bangumiAccNum 番剧数 默认3
 * @params[Number] specialAccNum 默认0
 * @params[Number] upuserAccNum up主数 默认0
 * @params[Number] tagNum 请求地址
 * @params[String] term 关键字
 * @params[Number] rnd 随机数
 * @return [Promise]
 */
function apiGetSearchSuggest ({
  url = '/search/getSuggest',
  func = 'suggest',
  suggestType = 'accurate',
  subType = 'tag',
  mainVer = 'v1',
  highlight = '',
  bangumiAccNum = 3,
  specialAccNum = 0,
  upuserAccNum = 0,
  tagNum = 10,
  term,
  rnd = Math.random()
} = {}) {
  return request.get(url, {
    data: {
      func,
      suggest_type: suggestType,
      sub_type: subType,
      main_ver: mainVer,
      highlight,
      bangumi_acc_num: bangumiAccNum,
      special_acc_num: specialAccNum,
      upuser_acc_num: upuserAccNum,
      tag_num: tagNum,
      term,
      rnd
    }
  })
}

/* 获取关键字搜索结果
 * @params[String] url 请求地址 可选 '/search/getResult' '/search/byKeyword'
 * @params[Object] data 请求参数
 * @return [Promise]
 */
function apiGetSearchResult ({ url = '/search/getResult', data = {} } = {}) {
  return request.get(url, { data })
}

export {
  apiGetDefaultKey,
  apiGetHotSearchKey,
  apiGetSearchSuggest,
  apiGetSearchResult
}
