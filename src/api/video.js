// 详情数据接口
import request from '@/utils/http.js'
/* 获取视频的详细信息
 * @params[String] url 请求地址
 * @params[String] bvid 视频bvid
 * @return [Promise]
 */
function apiGetDetail ({ url = '/video/getDetail', bvid } = {}) {
  return request.get(url, {
    data: {
      bvid
    }
  })
}

/* 获取视频的播放地址
 * @params[String] url 请求地址
 * @params[Number, String] avid 视频aid
 * @params[Number, String] bvid 视频bvid
 * @params[Number, String] cid 视频cid
 * @params[Number, String] platform 平台 默认h5
 * @params[Number, String] otype 返回数据格式 json|jsonp
 * @params[Number, String] qn 视频质量? 默认16
 * @params[Number, String] type 视频格式 默认mp4
 * @params[Number, String] html5 是否是html5页面请求 1|0
 * @return [Promise]
 */
function apiGetPlayUrl ({ url = '/video/getPlayUrl', aid, bvid, cid, platform = 'h5', otype = 'json', qn = '16', type = 'mp4', html5 = 1 } = {}) {
  return request.get(url, {
    data: {
      cid,
      avid: aid,
      bvid,
      platform,
      otype,
      qn,
      type,
      html5
    }
  })
}

/* 获取视频的标签信息
 * @params[String] url 请求地址
 * @params[Number, String] aid 视频aid
 * @params[Number, String] bvid 视频bvid
 * @return [Promise]
 */
function apiGetVideoTags ({ url = '/video/getTag', aid, bvid } = {}) {
  return request.get(url, {
    data: {
      aid,
      bvid
    }
  })
}

/* 获取视频的相关其他视频
 * @params[String] url 请求地址
 * @params[Number, String] aid 视频aid
 * @params[Number, String] bvid 视频bvid
 * @params[String] context 上下文? 默认''
 * @return [Promise]
 */
function apiGetVideoRelated ({ url = '/video/getRelated', aid, bvid, context = '' } = {}) {
  return request.get(url, {
    data: {
      aid,
      bvid,
      context
    }
  })
}

/* 获取视频的评论信息
 * @params[String] url 请求地址
 * @params[Number, String] aid 视频aid
 * @params[Number, String] bvid 视频bvid
 * @params[Number] type 类型? 默认1
 * @return [Promise]
 */
function apiGetVideoComment ({ url = '/video/getComment', aid, bvid, type = 1 } = {}) {
  return request.get(url, {
    data: {
      oid: aid,
      bvid,
      type
    }
  })
}

export {
  apiGetDetail,
  apiGetPlayUrl,
  apiGetVideoTags,
  apiGetVideoRelated,
  apiGetVideoComment
}
