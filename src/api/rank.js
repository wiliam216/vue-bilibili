// 首页数据接口
import request from '@/utils/http.js'
/* 获取排行榜数据
 * @params[String] url 请求地址
 * @params[Number] rid 类型id
 * @params[Number] day 统计天数 默认3
 * @return [Promise]
 */
function apiGetRank ({ url = '/rank/get', rid, day = 3 } = {}) {
  return request.get(url, {
    data: {
      rid,
      day
    }
  })
}

export {
  apiGetRank
}
