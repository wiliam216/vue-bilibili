// 投票页数据接口
import request from '@/utils/http.js'
/* 获取投票页数据
 * @params[String] url 请求地址
 * @params[Number] id 投票 id
 * @return [Promise]
 */
function apiGetInfo ({ url = '/vote/vote_info', id } = {}) {
  return request.get(url, {
    data: {
      vote_id: id
    }
  })
}

/* 获取评论数据
 * @params[String] url 请求地址
 * @params[Number] oid 评论所属的内容id
 * @params[Number] type 评论所属的内容类型id
 * @params[Number] mode 评论排序类型 2是按照时间，3是按照热度
 * @params[Number] next 分页的下一条记录的索引
 * @return [Promise]
 */
function apiGetComments ({ url = '/vote/comment', id, type = 28, mode = 2, next } = {}) {
  return request.get(url, {
    data: {
      oid: id,
      type,
      mode,
      next
    }
  })
}

export {
  apiGetInfo,
  apiGetComments
}
