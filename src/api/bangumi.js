// 番剧页数据接口
import request from '@/utils/http.js'

/* 获取视频基本信息
 * @params[String] url 请求地址
 * @params[Number] id 视频所属页面的id
 * @return [Promise]
 */
function apiGetInfo ({ url = '/bangumi/info', id } = {}) {
  return request.get(url, {
    data: {
      id
    }
  })
}

/* 获取剧集基本信息
 * @params[String] url 请求地址
 * @params[Number] seasonId 剧集id
 * @return [Promise]
 */
function apiGetSeason ({ url = '/bangumi/info', seasonId } = {}) {
  return request.get(url, {
    data: {
      season_id: seasonId
    }
  })
}

/* 获取视频播放地址
 * @params[String] url 请求地址
 * @params[Number] id 视频所属页面的id
 * @params[String] bsource
 * @return [Promise]
 */
function apiGetPlayUrl ({ url = '/bangumi/playurl', id, bsource } = {}) {
  return request.get(url, {
    data: {
      ep_id: id,
      bsource
    }
  })
}

/* 获取相关剧集
 * @params[String] url 请求地址
 * @params[Number] id 剧集id
 * @params[Number] fromPc 请求是否来源pc
 * @return [Promise]
 */
function apiGetRelated ({ url = '/bangumi/related', id, fromPc = 0 } = {}) {
  return request.get(url, {
    data: {
      season_id: id,
      from_pc: fromPc
    }
  })
}

/* 获取评论
 * @params[String] url 请求地址
 * @params[Number] type 评论类型 1为视频
 * @params[Number] sort 排序规则 2按时间 3按热度
 * @params[Number] id 评论所属页面的id
 * @params[Number] pn 页码
 * @params[Number] nohot 非热门评论
 * @return [Promise]
 */
function apiGetComment ({ url = '/bangumi/comment', type = 1, sort = 2, oid, pn, nohot = 1 } = {}) {
  return request.get(url, {
    data: {
      oid,
      type,
      sort,
      pn,
      nohot
    }
  })
}

export {
  apiGetInfo,
  apiGetSeason,
  apiGetPlayUrl,
  apiGetRelated,
  apiGetComment
}
