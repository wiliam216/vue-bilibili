// up主空间页数据接口
import request from '@/utils/http.js'

/* 获取用户的基本数据
 * @params[String] url 请求地址
 * @params[Number] mid up主的id
 * @params[String] jsonp 返回的数据格式 json|jsonp
 * @return [Promise]
 */
function apiGetUserInfo ({ url = '/space/getUserInfo', mid, jsonp = 'json' } = {}) {
  return request.get(url, {
    data: {
      mid,
      jsonp
    }
  })
}

/* 获取用户的社交数据 粉丝数+关注数
 * @params[String] url 请求地址
 * @params[Number] mid up主的id
 * @return [Promise]
 */
function apiGetUserStat ({ url = '/space/getUserStat', mid } = {}) {
  return request.get(url, {
    data: {
      vmid: mid
    }
  })
}

/* 获取用户获得的点赞数
 * @params[String] url 请求地址
 * @params[Number] mid up主的id
 * @return [Promise]
 */
function apiGetUserUpStat ({ url = '/space/getUserUpStat', mid } = {}) {
  return request.get(url, {
    data: {
      mid
    }
  })
}

/* 获取用户的动态数据
 * @params[String] url 请求地址
 * @params[Number] hostUid up主的id
 * @params[Number, String] offsetDynamicId 当前返回的数据最后一条记录的 动态id
 * @params[Number] needTop 是否置顶 默认1，可选0
 * @return [Promise]
 */
function apiGetPosts ({ url = '/space/getUserPosts', hostUid, offsetDynamicId, needTop = 1 } = {}) {
  return request.get(url, {
    data: {
      host_uid: hostUid,
      offset_dynamic_id: offsetDynamicId,
      need_top: needTop
    }
  })
}

/* 获取up主的视频
 * @params[String] url 请求地址
 * @params[Number] mid up主的id
 * @params[String] order 排序规则 默认按点击量click
 * @params[String] keyword 关键字 默认''
 * @params[Number] pn 当前页 默认1
 * @params[Number] ps 每页显示的记录数 默认100
 * @return [Promise]
 */
function apiGetVideos ({ url = '/space/getUserVideos', mid, order = 'click', keyword = '', pn = 1, ps = 100 } = {}) {
  return request.get(url, {
    data: {
      pn,
      ps,
      order,
      keyword,
      mid
    }
  })
}

/* 获取up主的相薄
 * @params[String] url 请求地址
 * @params[Number] uid up主的id
 * @return [Promise]
 */
function apiGetPhotos ({ url = '/space/getUserPhotos', uid } = {}) {
  return request.get(url, {
    data: {
      uid
    }
  })
}

export {
  apiGetUserInfo,
  apiGetUserStat,
  apiGetUserUpStat,
  apiGetPosts,
  apiGetVideos,
  apiGetPhotos
}
