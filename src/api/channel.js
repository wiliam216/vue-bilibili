// 首页数据接口
import request from '@/utils/http.js'
/* 获取频道页面数据
 * @params[String] url 请求地址
 * @params[Number] day 天数默认 7
 * @params[Number] rid 频道id
 * @params[String] context 上下文
 * @return [Promise]
 */
function apiGetRanking ({ url = '/channel/ranking', day = 7, rid, context = '' } = {}) {
  return request.get(url, {
    data: {
      rid,
      day,
      context
    }
  })
}
/* 获取频道页面数据
 * @params[String] url 请求地址
 * @params[Number] id 频道id
 * @params[Number] subid 下级频道id
 * @params[Number] pn 当前页
 * @return [Promise]
 */
function apiGetDynamic ({ url = '/channel/dynamic', pn = 1, id, subid, ps = 6 } = {}) {
  return request.get(url, {
    data: {
      id,
      rid: subid,
      pn,
      ps
    }
  })
}

export {
  apiGetRanking,
  apiGetDynamic
}
