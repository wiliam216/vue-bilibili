// 文章页数据接口
import request from '@/utils/http.js'

/* 获取文章正文内容
 * @params[String] url 请求地址
 * @params[String] jsonp 返回的数据格式 默认 jsonp
 * @params[Number] id 文章id
 * @return [Promise]
 */
function apiGetArticleContent ({ url = '/article/content', jsonp = 'jsonp', id } = {}) {
  return request.get(url, {
    data: {
      id
    }
  })
}
/* 获取文章其他相关信息
 * @params[String] url 请求地址
 * @params[String] jsonp 返回的数据格式 默认 jsonp
 * @params[Number] id 文章id
 * @return [Promise]
 */
function apiGetArticleAbout ({ url = '/article/about', jsonp = 'jsonp', id, mobiApi = 'h5', from } = {}) {
  return request.get(url, {
    data: {
      jsonp,
      mobi_api: mobiApi,
      from,
      id
    }
  })
}
/* 获取更多推荐文章
 * @params[String] url 请求地址
 * @params[String] jsonp 返回的数据格式 默认 jsonp
 * @params[Number] id 文章id
 * @return [Promise]
 */
function apiGetMoreArticle ({ url = '/article/more', jsonp = 'jsonp', id } = {}) {
  return request.get(url, {
    data: {
      jsonp,
      aid: id
    }
  })
}

/* 获取文章内容里面的视频
 * @params[String] url 请求地址
 * @params[String] jsonp 返回的数据格式 默认 jsonp
 * @params[Number] id 文章id
 * @params[String] videoIds 视频ids
 * @return [Promise]
 */
function apiGetVideos ({ url = '/article/videos', jsonp = 'jsonp', id, videoIds } = {}) {
  return request.get(url, {
    data: {
      jsonp,
      id: id,
      ids: videoIds
    }
  })
}

/* 获取更多推荐文章
 * @params[String] url 请求地址
 * @params[String] type 类型 默认 12
 * @params[Number] id 文章id
 * @return [Promise]
 */
function apiGetComments ({ url = '/article/comments', type = 12, id } = {}) {
  return request.get(url, {
    data: {
      type,
      oid: id
    }
  })
}

export {
  apiGetArticleContent,
  apiGetArticleAbout,
  apiGetMoreArticle,
  apiGetVideos,
  apiGetComments
}
