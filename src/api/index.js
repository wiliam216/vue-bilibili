// 首页数据接口
import request from '@/utils/http.js'
/* 获取首页轮播图
 * @params[String] url 请求地址
 * @params[String] jsonp 返回的数据格式 默认 jsonp
 * @params[Number] pf 默认 7
 * @params[Number] id 默认 1695
 * @return [Promise]
 */
function apiGetSlider ({ url = '/index/getSlider', jsonp = 'jsonp', pf = 7, id = 1695 } = {}) {
  return request.get(url, {
    data: {
      jsonp,
      pf,
      id
    }
  })
}

/* 获取首页数据
 * @params[String] url 请求地址
 * @params[String] jsonp 返回的数据格式 默认 jsonp
 * @params[Number] day 返回最近3天的 默认 3
 * @params[Number] rid 默认 0
 * @return [Promise]
 */
function apiGetRecommend ({ url = '/index/getRecommend', jsonp = 'jsonp', day = 3, rid = 0 } = {}) {
  return request.get(url, {
    data: {
      jsonp,
      day,
      rid
    }
  })
}

export {
  apiGetSlider,
  apiGetRecommend
}
