// 详情数据接口
import request from '@/utils/http.js'

/* 获取标签的基本信息
 * @params[String] url 请求地址
 * @params[Number, String] tid 标签id
 * @return [Promise]
 */
function apiGetTagInfo ({ url = '/tag/getInfo', tid } = {}) {
  return request.get(url, {
    data: {
      tag_id: tid
    }
  })
}

/* 获取标签的相关标签
 * @params[String] url 请求地址
 * @params[Number, String] tid 标签id
 * @return [Promise]
 */
function apiGetTagRelated ({ url = '/tag/getSimilar', tid } = {}) {
  return request.get(url, {
    data: {
      tag_id: tid
    }
  })
}

/* 获取标签的相关视频
 * @params[String] url 请求地址
 * @params[Number, String] tid 标签id
 * @params[Number] pn 当前页 默认1
 * @params[Number] ps 每页显示记录数 默认20
 * @return [Promise]
 */
function apiGetTagVideos ({ url = '/tag/getVideos', tid, pn = 1, ps = 20 } = {}) {
  return request.get(url, {
    data: {
      tid,
      pn,
      ps
    }
  })
}
export {
  apiGetTagInfo,
  apiGetTagRelated,
  apiGetTagVideos
}
