# vue-bilibili
> 这是一个小的项目，仿照B站H5端的页面，使用Vue-Cli3 + Vant搭建前台展示页面，Koa + koa-router搭建后台服务端代理请求抓取b站数据
## 安装准备及本地运行
```
# 克隆项目
git clone https://gitee.com/xhxno36/vue-bilibili.git

# 进入项目目录
cd vue-bilibili

# 安装依赖
npm install

# 本地运行
npm run dev

# 开启后端服务代理
npm run server
```

## 打包上线
```
# 打包压缩代码
npm run build

# 预览发布环境效果
npm run preview

# 预览发布环境效果 + 静态资源分析
npm run report
```

## 目录结构说明
+ build 生成打包效果 + 静态资源分析
+ public 模板页
+ server 后端代理请求（抓取b站页面的设置）
+ src
   + api 页面的请求
   + assets 静态资源
   + componenst 组件
   + filters 公用的vue过滤器
   + mixins 公用的vue混入
   + router 路由配置   
   + store Vuex的配置
   + styles 公用样式
   + utils 公用的工具脚本
   + views 页面
   + App.vue 项目的根组件
   + main.js 项目的入口文件
+ vue.config.js Vue-Cli3的开发环境和生产环境配置

## 功能说明
+ 已实现的功能
    + 首页，视频详情页，番剧详情页，搜索页，文章页，空间页，标签详情页，投票页，排行榜页，话题页，各个频道页的内容展示

+ 未实现的功能
    + 会员登录功能，评论功能，一键三连的功能
    + 弹幕播放器
    + 所有涉及会员操作的功能


