/* const path = require('path')
function resolve (dir) {
  return path.join(__dirname, dir)
} */

module.exports = {
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'assets',
  lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,
  devServer: {
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        target: `${process.env.VUE_APP_SERVER_HOST}/`,
        secure: false,
        changeOrigin: true
      }
    }
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: `@import "~@/styles/var.scss";
        @import "~@/styles/mixin.scss";
        @import "~@/styles/function.scss";
        `
      }
    }
  },
  chainWebpack (config) {
    config.plugins.delete('preload')
    config.plugins.delete('prefetch')

    config.when(process.env.NODE_ENV === 'development', config => config.devtool('cheap-source-map'))

    // 修改vue-cli3关于图片打包的配置，设定打包限制为10kb 默认4kb
    /* config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
      .options({
        limit: 10240
      }) */
  }
}
